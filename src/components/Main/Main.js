import { useEffect, useState } from 'react';
import UserDetails from '../UserDetails/UserDetails';
import UserList from '../UserList/UserList';
import styles from './Main.module.css';

const Main = () => {
    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);

    const getData = async () => {
        // api request
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        const data = await response.json();

        console.log(data);
        setUsers(data);
    }

    const onUserSelected = (user) => {
        setSelectedUser(user);
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className={styles.main}>
            <UserList 
                users={users}
                onUserSelected={onUserSelected}
            />
            <UserDetails 
                user={selectedUser}
            />
        </div>
    );
}

export default Main;

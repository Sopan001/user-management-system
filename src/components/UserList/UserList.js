import User from './User/User';
import styles from './UserList.module.css';

const UserList = ({ users, onUserSelected }) => {
    return (
        <section className={styles["user-list"]}>
            {
                users.map(u => <User 
                    onUserSelected={() => {
                        onUserSelected(u);
                    }}
                    key={u.id} 
                    name={u.name}
                />)
            }
        </section>
    )
}

export default UserList;
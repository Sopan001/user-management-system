import styles from './User.module.css';

const User = ({ name, onUserSelected }) => {
    return (
        <div onClick={onUserSelected} className={styles.user}>
            <span>{name}</span>
        </div>
    )
}

export default User;
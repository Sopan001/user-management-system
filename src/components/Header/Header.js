import styles from './Header.module.css';

const Header = () => {
    return (
        <header className={styles.header}>
            <h1>User Management</h1>
        </header>
    )
}

export default Header;